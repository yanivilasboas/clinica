# Clínica Vilas Boas

Sistema para o cadastro de pacientes para uma clínica


Este arquivo está dividido em:

##Passos para Instalação da Aplicação
##Informações Adicionais Sobre a Aplicação

##Passos para Instalação da Aplicação

Para funcionamento do projeto, basta criar um banco de dados com o nome clinicavb e executar o código Java. Lembrando de alterar a senha no arquivo "application.properties". Ao abrir o front-end, executar o comando yarn para baixar dependências e em seguida, executar o projeto com yarn start. A tela de cadastro de médicos (usuário) está pública, portanto para utilizar o sistema, basta acessar http://localhost:3000/medico, criar um usuário, realizar login (http://localhost:3000/) e ver as demais funcionalidades do sistema.

##Informações Adicionais Sobre a Aplicação

Sistema desenvolvido com Spring Boot e React, utilizando os softwares VSCode e Intellij IDEA. Para estilização da aplicação, foi-se usado a biblioteca Ant Design, Sass e CSS. O banco de dados utilizado foi PostgreSQL.

O projeto não está responsivo e utilizo o zoom da minha página em 80%.

O sistema da *Clinica Vilas Boas* tem a capacidade de gerenciar os pacientes e enfermeiros. O usuário, médico, pode cadastrar, listar, atualizar e excluir dados de Pacientes e Enfermeiros. Nas listagens, o usuário pode realizar buscas por termo de pesquisa, sendo que os pacientes podem ser filtrados por nome ou UF. Os cadastros dos médicos só podem ser alterados ou excluído pelo administrador do banco. Há um vídeo que mostra o projeto em execução, nomeado como "apresentação, que também pode ser acessado no link https://drive.google.com/file/d/1H3GWlwnVZfEnaS9eaN9G-AzD31My7YxQ/view?usp=sharing


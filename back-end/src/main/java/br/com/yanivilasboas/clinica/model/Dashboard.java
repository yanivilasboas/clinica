package br.com.yanivilasboas.clinica.model;


import lombok.Data;

@Data
public class Dashboard {

    private Long qtdEnfermeiros;
    private Long qtdMedicos;
    private Long qtdPacientes;
}

package br.com.yanivilasboas.clinica.controller;

import br.com.yanivilasboas.clinica.model.Paciente;
import br.com.yanivilasboas.clinica.config.Config;
import br.com.yanivilasboas.clinica.repository.PacienteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/publico/pacientes")
@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
public class PacienteController {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    private PacienteRepository pacienteRepository;

    @GetMapping
    public List<Paciente> listarTodos(){
        return pacienteRepository.findAll();
    }

    @GetMapping("/{idPaciente}")
    public Paciente buscarpeloId(@PathVariable Long idPaciente) {
        return pacienteRepository.findById(idPaciente).orElse(null);
    }

    @DeleteMapping("/{idPaciente}")
    public void remover(@PathVariable Long idPaciente) {
        pacienteRepository.deleteById(idPaciente);
    }

    @PostMapping
    public Paciente cadastrar(@RequestBody Paciente paciente ){

        paciente.setCpf(passwordEncoder.encode(paciente.getCpf() + Config.salt));
        return pacienteRepository.save(paciente);
    }

    @PutMapping
    public Paciente atualizar(@RequestBody Paciente paciente) {
        return pacienteRepository.save(paciente);
    }
}

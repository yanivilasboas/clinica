package br.com.yanivilasboas.clinica.repository;

import br.com.yanivilasboas.clinica.model.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PacienteRepository extends JpaRepository<Paciente, Long> {
}

package br.com.yanivilasboas.clinica.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Data
public class Paciente {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idPaciente;

    private String cpf;

    private String nome;

    private LocalDate data_nascimento;

    private Double peso;

    private Double altura;

    private String uf;
}
